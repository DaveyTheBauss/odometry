package org.firstinspires.ftc.teamcode.drive.opmode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.acmerobotics.roadrunner.trajectory.TrajectoryBuilder;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.drive.StandardTrackingWheelLocalizer;
import org.firstinspires.ftc.teamcode.trajectorysequence.TrajectorySequence;
import org.firstinspires.ftc.teamcode.util.Encoder;

@TeleOp(name="DistanceTest", group="Linear Opmode")

public class OdometryTest extends LinearOpMode {

    private Encoder leftEncoder, rightEncoder, frontEncoder;

    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        StandardTrackingWheelLocalizer e = new StandardTrackingWheelLocalizer(hardwareMap);

        double currentPositionX;
        double currentPositionY;
        double direction = 90; // change when otherwise

        leftEncoder = new Encoder(hardwareMap.get(DcMotorEx.class, "BackRight")); //right
        rightEncoder = new Encoder(hardwareMap.get(DcMotorEx.class, "FrontRight"));
        frontEncoder = new Encoder(hardwareMap.get(DcMotorEx.class, "FrontLeft"));

        rightEncoder.setDirection(Encoder.Direction.REVERSE);

        //drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        waitForStart();

        drive.setWeightedDrivePower(
                new Pose2d(
                        -gamepad1.left_stick_y,
                        -gamepad1.left_stick_x,
                        -gamepad1.right_stick_x
                )
        );

        while(direction >= 360){
            direction = direction - 360;
        }
        
        while (opModeIsActive()) {
            currentPositionX = drive.getPoseEstimate().getX();
            currentPositionY = drive.getPoseEstimate().getY();
            if (gamepad1.y) {
                Trajectory forward = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .forward(24)
                        .build();
                drive.update();
                drive.followTrajectory(forward);

                currentPositionX = drive.getPoseEstimate().getX();
                currentPositionY = drive.getPoseEstimate().getY();
            }
            if (gamepad1.a) {
                Trajectory back = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .back(24)
                        .build();
                drive.update();
                drive.followTrajectory(back);

                currentPositionX = drive.getPoseEstimate().getX();
                currentPositionY = drive.getPoseEstimate().getY();
            }
            if (gamepad1.x) {
                Trajectory left = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .strafeLeft(24)
                        .build();
                drive.update();
                drive.followTrajectory(left);

                currentPositionX = drive.getPoseEstimate().getX();
                currentPositionY = drive.getPoseEstimate().getY();
            }
            if (gamepad1.b) {
                Trajectory right = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .strafeRight(24)
                        .build();
                drive.update();
                drive.followTrajectory(right);

                currentPositionX = drive.getPoseEstimate().getX();
                currentPositionY = drive.getPoseEstimate().getY();
            }
            if (gamepad1.right_bumper) {
                TrajectorySequence vector = drive.trajectorySequenceBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .strafeTo(new Vector2d(0, 0))
                        .turn(Math.toRadians(direction))
                        .build();
                
                drive.update();
                drive.followTrajectorySequence(vector);

                currentPositionX = drive.getPoseEstimate().getX();
                currentPositionY = drive.getPoseEstimate().getY();
            }
            if (gamepad1.left_bumper){
                double oldx = currentPositionX;
                double oldy = currentPositionX;
                TrajectorySequence speen = drive.trajectorySequenceBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .turn(Math.toRadians(direction))
                        .build();
                drive.update();
                drive.followTrajectorySequence(speen);

                currentPositionX = oldx;
                currentPositionY = oldy;
            }
            drive.update();
            idle();

            telemetry.addData("X", currentPositionX);
            telemetry.addData("Y" , currentPositionY);
            telemetry.addData("Velocity(Wheels)" , drive.getWheelVelocities());
            telemetry.addData("Direction" , direction);
            telemetry.update();

        }


    }
}