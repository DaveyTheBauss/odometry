package org.firstinspires.ftc.teamcode.drive.opmode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.acmerobotics.roadrunner.trajectory.TrajectoryBuilder;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.drive.StandardTrackingWheelLocalizer;
import org.firstinspires.ftc.teamcode.trajectorysequence.TrajectorySequence;
import org.firstinspires.ftc.teamcode.util.Encoder;

@TeleOp(name="SampleAutonPath", group="Linear Opmode")

public class SampleAutonPath extends LinearOpMode {

    private Encoder leftEncoder, rightEncoder, frontEncoder;

    @Override
    public void runOpMode() throws InterruptedException {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        StandardTrackingWheelLocalizer e = new StandardTrackingWheelLocalizer(hardwareMap);

        double currentPositionX = 0.0;
        double currentPositionY = 0.0;

        leftEncoder = new Encoder(hardwareMap.get(DcMotorEx.class, "BackRight")); //right
        rightEncoder = new Encoder(hardwareMap.get(DcMotorEx.class, "FrontRight"));
        frontEncoder = new Encoder(hardwareMap.get(DcMotorEx.class, "FrontLeft"));

        rightEncoder.setDirection(Encoder.Direction.REVERSE);

        //drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);


        Trajectory gotoe = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                .forward(24)
                .build();
        /*Trajectory back = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                .back(24)
                .build();
        Trajectory left = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                .strafeLeft(24)
                .build();
        Trajectory right = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
                .strafeRight(24)
                .build();*/

        /*TrajectorySequence speen = drive.trajectorySequenceBuilder(new Pose2d())
                .turn(Math.toRadians(90))
                .build();*/

        waitForStart();

        drive.setWeightedDrivePower(
                new Pose2d(
                        -gamepad1.left_stick_y,
                        -gamepad1.left_stick_x,
                        -gamepad1.right_stick_x
                )
        );

        while (opModeIsActive()) {
            currentPositionX = drive.getPoseEstimate().getX();
            currentPositionY = drive.getPoseEstimate().getY();
            if (gamepad1.y) {
                drive.update();
//                drive.followTrajectory(forward);
//                drive.followTrajectorySequence();
            }
            if (gamepad1.a) {
//                drive.update();
//                drive.followTrajectory(back);
            }
            if (gamepad1.x) {
//                drive.update();
//                drive.followTrajectory(left);
            }
            if (gamepad1.b) {
//                drive.update();
//                drive.followTrajectory(right);
                TrajectorySequence skystonemoment = drive.trajectorySequenceBuilder(new Pose2d(currentPositionX, currentPositionY))
                        .strafeTo(new Vector2d(24,24))
                        .forward(20)
                        .waitSeconds(1)
                        .strafeRight(10)
                        .turn(Math.toRadians(90))
                        .waitSeconds(1)
                        .strafeTo(new Vector2d(0,0))
                        .build();
                drive.followTrajectorySequence(skystonemoment);
            }
            if (gamepad1.right_bumper) {
//                Trajectory returne = drive.trajectoryBuilder(new Pose2d(currentPositionX,currentPositionY))
//                        .strafeTo(new Vector2d(0, 0))
//                        .build();

                TrajectorySequence vector = drive.trajectorySequenceBuilder(new Pose2d(currentPositionX,currentPositionY))
                        .strafeTo(new Vector2d(40, 24))
                        .waitSeconds(1)
                        .strafeTo(new Vector2d(0, 0))
                        .build();
//
                drive.update();
                drive.followTrajectorySequence(vector);
            }
            if (gamepad1.left_bumper){
//                drive.update();
//                drive.followTrajectorySequence(speen);
            }
            drive.update();
            idle();

            telemetry.addData("X", currentPositionX);
            telemetry.addData("Y" , currentPositionY);
            telemetry.addData("Velocity(Wheels)" , drive.getWheelVelocities());
            telemetry.update();

        }


    }
}