package org.firstinspires.ftc.teamcode.drive.opmode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.drive.StandardTrackingWheelLocalizer;
import org.firstinspires.ftc.teamcode.drive.opmode.PIDController;
import org.firstinspires.ftc.teamcode.util.Encoder;

public class WheelsWithOdometry {
    public enum Direction {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT
    }

    private DcMotor frontLeft  = null;
    private DcMotor frontRight = null;
    private DcMotor backLeft   = null;
    private DcMotor backRight  = null;

    private Encoder leftEncoder, rightEncoder, frontEncoder;

    BNO055IMU               imu;
    Orientation             lastAngles = new Orientation();
    double                  globalAngle, correction, rotation;
    PIDController pidRotate, pidDrive;

    HardwareMap   hwMap = null;
    LinearOpMode  opMode;

    static final boolean RUN_WITH_SCALE_FACTOR = false;

    static final double  COUNTS_PER_MOTOR_REV  = 538; // 5202 GoBilda Yellow Jacket Encoder
    // NEED TO FIND OUT
    static final double  DRIVE_GEAR_RATIO      = 1.209;  // Gear on Motor : Gear on Wheel
    // E.g., 3:1 means 3x larger gear on Motor,
    // One motor rotation results 3 wheel rotations.
    // NEED TO FIND OUT
    static final double  WHEEL_DIAMETER_INCHES = 4.0;  // Wheel circumference

    // These constants define the desired driving/control characteristics
    // The can/should be tweaked to suite the specific robot drive train.
    static final double  DRIVE_SPEED_FORWARD        = 0.80;  // Drive forward speed 0.8
    static final double  DRIVE_SPEED_BACKWARD       = 0.80;  // Drive backward speed 0.8
    static final double  DRIVE_SPEED_LEFT           = 0.90;  // Drive left speed 0.9
    static final double  DRIVE_SPEED_RIGHT          = 0.90;  // Drive right speed 0.9

    // Function to check if opModeIsActive
    private boolean opModeIsActive() {
        return opMode.opModeIsActive();
    }

    // Function to add telemetry data
    private void telemetryAddData(String sDataItem, String sData) {
        opMode.telemetry.addData(sDataItem, sData);
    }

    // Function to update telemetry
    private void telemetryUpdate() {
        opMode.telemetry.update();
    }

    public WheelsWithOdometry(LinearOpMode opmode) {
        this.opMode = opmode;
    }

    public void init(HardwareMap ahwMap) {
        StandardTrackingWheelLocalizer e = new StandardTrackingWheelLocalizer(ahwMap);
        SampleMecanumDrive sam = new SampleMecanumDrive(ahwMap);

        hwMap = ahwMap;

        frontLeft  = hwMap.get(DcMotor.class, "FrontLeft");
        frontRight = hwMap.get(DcMotor.class, "FrontRight");
        backLeft   = hwMap.get(DcMotor.class, "BackLeft");
        backRight  = hwMap.get(DcMotor.class, "BackRight");

        frontLeft.setDirection(DcMotor.Direction.FORWARD);
        frontRight.setDirection(DcMotor.Direction.FORWARD);
        backLeft.setDirection(DcMotor.Direction.REVERSE);
        backRight.setDirection(DcMotor.Direction.REVERSE);

        leftEncoder = new Encoder(hwMap.get(DcMotorEx.class, "BackRight"));
        rightEncoder = new Encoder(hwMap.get(DcMotorEx.class, "FrontLeft"));
        frontEncoder = new Encoder(hwMap.get(DcMotorEx.class, "FrontRight"));

        // TODO: reverse any encoders using Encoder.setDirection(Encoder.Direction.REVERSE)
        frontEncoder.setDirection(Encoder.Direction.REVERSE);
        rightEncoder.setDirection(Encoder.Direction.REVERSE);

        frontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();

        parameters.mode                = BNO055IMU.SensorMode.IMU;
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.loggingEnabled      = false;

        // Retrieve and initialize the IMU. We expect the IMU to be attached to an I2C port
        // on a Core Device Interface Module, configured to be a sensor of type "AdaFruit IMU",
        // and named "imu".
        imu = hwMap.get(BNO055IMU.class, "imu");

        imu.initialize(parameters);

        // Set PID proportional value to start reducing power at about 50 degrees of rotation.
        // P by itself may stall before turn completed so we add a bit of I (integral) which
        // causes the PID controller to gently increase power if the turn is not completed.
        pidRotate = new PIDController(.003, .00003, 0);

        // Set PID proportional value to produce non-zero correction value when robot veers off
        // straight line. P value controls how sensitive the correction is.
        pidDrive = new PIDController(.05, 0, 0);

        telemetryAddData("Mode", "calibrating...");
        telemetryUpdate();

        // make sure the imu gyro is calibrated before continuing.
        while (!opMode.isStopRequested() && !imu.isGyroCalibrated())
        {
            opMode.sleep(50);
            opMode.idle();
        }

        // Reset imu heading angle
        resetAngle();

        telemetryAddData("imu calib status", imu.getCalibrationStatus().toString());
        telemetryUpdate();
    }

    /**
     * Resets the cumulative angle tracking to zero.
     */
    private void resetAngle()
    {
        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        globalAngle = 0;
    }

    /**
     * Get current cumulative angle rotation from last reset.
     * @return Angle in degrees. + = left, - = right from zero point.
     */
    private double getAngle()
    {
        // We experimentally determined the Z axis is the axis we want to use for heading angle.
        // We have to process the angle because the imu works in euler angles so the Z axis is
        // returned as 0 to +180 or 0 to -180 rolling back to -179 or +179 when rotation passes
        // 180 degrees. We detect this transition and track the total cumulative angle of rotation.

        Orientation angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;

        if (deltaAngle < -180)
            deltaAngle += 360;
        else if (deltaAngle > 180)
            deltaAngle -= 360;

        globalAngle += deltaAngle;

        lastAngles = angles;

        return globalAngle;
    }


    static final double  HEADING_THRESHOLD     = 1 ;   // As tight as we can make it with an integer gyro
    static final double  P_TURN_COEFF          = 0.1;  // Larger is more responsive, but also less stable

    /**
     * Perform one cycle of closed loop heading control.
     *
     * @param speed     Desired speed of turn.
     * @param angle     Absolute Angle (in Degrees) relative to last gyro reset.
     *                  0 = fwd. +ve is CCW from fwd. -ve is CW from forward.
     *                  If a relative angle is required, add/subtract from current heading.
     * @param PCoeff    Proportional Gain coefficient
     * @return
     */
    private boolean onHeading(double speed, double angle, double PCoeff) {
        double   error ;
        double   steer ;
        boolean  onTarget = false ;
        double   leftSpeed;
        double   rightSpeed;
        String   sData;

        // determine turn power based on +/- error
        error = getError(angle);

        if (Math.abs(error) <= HEADING_THRESHOLD) {
            steer = 0.0;
            leftSpeed  = 0.0;
            rightSpeed = 0.0;
            onTarget = true;
        } else {
            steer = getSteer(error, PCoeff);
            leftSpeed   = -speed * steer;
            rightSpeed  = speed * steer;
        }

        // Send desired speeds to motors.
        frontLeft.setPower(leftSpeed);
        frontRight.setPower(rightSpeed);
        backLeft.setPower(leftSpeed);
        backRight.setPower(rightSpeed);

        // Display it for the driver.
        //telemetry.addData("Target", "%5.2f", angle);
        //telemetry.addData("Err/St", "%5.2f/%5.2f", error, steer);
        //telemetry.addData("Speed.", "%5.2f:%5.2f", leftSpeed, rightSpeed);
        sData = String.format("%5.2f", angle);
        telemetryAddData("Target", sData);
        sData = String.format("%5.2f/%5.2f", error, steer);
        telemetryAddData("Err/St", sData);
        sData = String.format("%5.2f:%5.2f", leftSpeed, rightSpeed);
        telemetryAddData("Speed.", sData);

        return onTarget;
    }

    /**
     * getError determines the error between the target angle and the robot's current heading
     * @param   targetAngle  Desired angle (relative to global reference established at last Gyro Reset).
     * @return  error angle: Degrees in the range +/- 180. Centered on the robot's frame of reference
     *          +ve error means the robot should turn LEFT (CCW) to reduce error.
     */
    private double getError(double targetAngle) {
        double robotError;

        // calculate error in -179 to +180 range  (
        robotError = targetAngle - getAngle();
        while (robotError > 180)  robotError -= 360;
        while (robotError <= -180) robotError += 360;
        return robotError;
    }

    /**
     * returns desired steering force.  +/- 1 range.  +ve = steer left
     * @param error   Error angle in robot relative degrees
     * @param PCoeff  Proportional Gain Coefficient
     * @return
     */
    private double getSteer(double error, double PCoeff) {
        return Range.clip(error * PCoeff, -1, 1);
    }
    /**
     * Rotate left or right the number of degrees. Does not support turning more than 359 degrees.
     * @param degrees Degrees to turn, + is left - is right
     */

    public void rotate(int degrees, double speed)
    {
        resetAngle();
        // if degrees > 359 we cap at 359 with same sign as original degrees.
        if (Math.abs(degrees) > 359) degrees = (int) Math.copySign(359, degrees);

        // getAngle() returns + when rotating ccw (left) and - when rotating cw (right)

        // keep looping while we are still active, and not on heading.
        while (opModeIsActive() && !onHeading(speed, degrees, P_TURN_COEFF)) {
            // Update telemetry & Allow time for other processes to run.
            telemetryUpdate();
        }
        resetAngle();
    }

    /**
     * Rotate left or right the number of degrees. Does not support turning more than 359 degrees.
     * @param degrees Degrees to turn, + is left - is right
     */
    public void rotateWithPID(int degrees, double power)
    {
        // restart imu angle tracking.
        resetAngle();

        // if degrees > 359 we cap at 359 with same sign as original degrees.
        if (Math.abs(degrees) > 359) degrees = (int) Math.copySign(359, degrees);

        // start pid controller. PID controller will monitor the turn angle with respect to the
        // target angle and reduce power as we approach the target angle. This is to prevent the
        // robots momentum from overshooting the turn after we turn off the power. The PID controller
        // reports onTarget() = true when the difference between turn angle and target angle is within
        // 1% of target (tolerance) which is about 1 degree. This helps prevent overshoot. Overshoot is
        // dependant on the motor and gearing configuration, starting power, weight of the robot and the
        // on target tolerance. If the controller overshoots, it will reverse the sign of the output
        // turning the robot back toward the setpoint value.

        pidRotate.reset();
        pidRotate.setSetpoint(degrees);
        pidRotate.setInputRange(0, degrees);
        pidRotate.setOutputRange(0, power);
        pidRotate.setTolerance(1);
        pidRotate.enable();

        // getAngle() returns + when rotating counter clockwise (left) and - when rotating
        // clockwise (right).

        // rotate until turn is completed.

        if (degrees < 0)
        {
            // On right turn we have to get off zero first.
            while (opModeIsActive() && getAngle() == 0)
            {
                frontLeft.setPower(power);
                frontRight.setPower(-power);
                backLeft.setPower(power);
                backRight.setPower(-power);
                opMode.sleep(100);
            }

            do
            {
                power = pidRotate.performPID(getAngle()); // power will be - on right turn.
                frontLeft.setPower(-power);
                frontRight.setPower(power);
                backLeft.setPower(-power);
                backRight.setPower(power);
            } while (opModeIsActive() && !pidRotate.onTarget());
        }
        else    // left turn.
            do
            {
                power = pidRotate.performPID(getAngle()); // power will be + on left turn.
                frontLeft.setPower(-power);
                frontRight.setPower(power);
                backLeft.setPower(-power);
                backRight.setPower(power);
            } while (opModeIsActive() && !pidRotate.onTarget());

        // turn the motors off.
        frontLeft.setPower(0);
        frontRight.setPower(0);
        backLeft.setPower(0);
        backRight.setPower(0);

        rotation = getAngle();

        // wait for rotation to stop.
        opMode.sleep(500);

        // reset angle tracking on new heading.
        resetAngle();
    }

    public void forwardByInches(double inches) {
        runWithBuiltInEncoder(DRIVE_SPEED_FORWARD, inches, Direction.FORWARD);
    }

    public void reverseByInches(double inches) {
        runWithBuiltInEncoder(DRIVE_SPEED_BACKWARD, inches, Direction.BACKWARD);

    }

    public void slideLeftByInches(double inches) {
        runWithBuiltInEncoder(DRIVE_SPEED_LEFT, inches, Direction.LEFT);
    }

    public void slideRightByInches(double inches) {
        runWithBuiltInEncoder(DRIVE_SPEED_RIGHT, inches, Direction.RIGHT);
    }

    private void runWithBuiltInEncoder(double speed, double distance, Direction direction) {
        double initAngle = getAngle();

        // Ready to use motor encoders
        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Set up parameters for driving in a straight line.
        pidDrive.setSetpoint(0);
        pidDrive.setOutputRange(0, speed);
        pidDrive.setInputRange(-90, 90);
        pidDrive.enable();

        int     newFrontLeftTarget  = 0;
        int     newFrontRightTarget = 0;
        int     newBackLeftTarget   = 0;
        int     newBackRightTarget  = 0;
        int     moveCounts = 0;
        double  max;
        double  frontLeftSpeed  = 0;
        double  frontRightSpeed = 0;
        double  backLeftSpeed   = 0;
        double  backRightSpeed  = 0;
        String  sData;

        // Ensure that the opmode is still active
        if (opModeIsActive() & (distance > 0)) {

            // Determine move counts for encoder
            moveCounts = (int)getMoveCount(distance, direction);

            // Determine new target position, and pass to motor controller
            if (direction == Direction.FORWARD) {
                newFrontLeftTarget  = frontLeft.getCurrentPosition()  + moveCounts;
                newFrontRightTarget = frontRight.getCurrentPosition() + moveCounts;
                newBackLeftTarget   = backLeft.getCurrentPosition()   + moveCounts;
                newBackRightTarget  = backRight.getCurrentPosition()  + moveCounts;
            } else if (direction == Direction.BACKWARD) {
                newFrontLeftTarget  = frontLeft.getCurrentPosition()  - moveCounts;
                newFrontRightTarget = frontRight.getCurrentPosition() - moveCounts;
                newBackLeftTarget   = backLeft.getCurrentPosition()   - moveCounts;
                newBackRightTarget  = backRight.getCurrentPosition()  - moveCounts;
            } else if (direction == Direction.LEFT) {
                newFrontLeftTarget  = frontLeft.getCurrentPosition()  - moveCounts;
                newFrontRightTarget = frontRight.getCurrentPosition() + moveCounts;
                newBackLeftTarget   = backLeft.getCurrentPosition()   + moveCounts;
                newBackRightTarget  = backRight.getCurrentPosition()  - moveCounts;
            } else if (direction == Direction.RIGHT) {
                newFrontLeftTarget  = frontLeft.getCurrentPosition()  + moveCounts;
                newFrontRightTarget = frontRight.getCurrentPosition() - moveCounts;
                newBackLeftTarget   = backLeft.getCurrentPosition()   - moveCounts;
                newBackRightTarget  = backRight.getCurrentPosition()  + moveCounts;
            }

            // Set Target
            frontLeft.setTargetPosition(newFrontLeftTarget);
            frontRight.setTargetPosition(newFrontRightTarget);
            backLeft.setTargetPosition(newBackLeftTarget);
            backRight.setTargetPosition(newBackRightTarget);

            // start motion.
            speed = Range.clip(Math.abs(speed), 0.0, 1.0);

            if (direction == Direction.FORWARD) { //forward
                frontLeftSpeed  = speed;
                frontRightSpeed = speed;
                backLeftSpeed   = speed;
                backRightSpeed  = speed;
            } else if (direction == Direction.BACKWARD) { //backward
                frontLeftSpeed  = -speed;
                frontRightSpeed = -speed;
                backLeftSpeed   = -speed;
                backRightSpeed  = -speed;
            } else if (direction == Direction.LEFT) { //left
                frontLeftSpeed  = -speed;
                frontRightSpeed = -/*+*/speed;
                backLeftSpeed   = speed;
                backRightSpeed  = -speed;
            } else if (direction == Direction.RIGHT) { //right
                frontLeftSpeed  = speed;
                frontRightSpeed = /*-*/speed;
                backLeftSpeed   = -speed;
                backRightSpeed  = speed;
            }

            // Set speed
            frontLeft.setPower(frontLeftSpeed);
            frontRight.setPower(frontRightSpeed);
            backLeft.setPower(backLeftSpeed);
            backRight.setPower(backRightSpeed);

            // Turn On RUN_TO_POSITION
            frontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            frontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            backLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            backRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            sData = String.format("%5.1f", initAngle);
            telemetryAddData("Angle", sData);
            sData = String.format("%7d:%7d:%7d:%7d", newFrontLeftTarget, newFrontRightTarget, newBackLeftTarget, newBackRightTarget);
            telemetryAddData("Target", sData);
            sData = String.format("%7d:%7d:%7d:%7d", frontLeft.getCurrentPosition(), frontRight.getCurrentPosition(),
                    backLeft.getCurrentPosition(), backRight.getCurrentPosition());

            telemetryAddData("current",sData);
            telemetryUpdate();

            // keep looping while we are still active, and BOTH motors are running.
            while (opModeIsActive() && frontRight.isBusy() && backRight.isBusy() && frontLeft.isBusy() && backLeft.isBusy()) {
                opMode.sleep(1);
            }

            // turn the motors off.
            frontLeft.setPower(0);
            frontRight.setPower(0);
            backLeft.setPower(0);
            backRight.setPower(0);

            //Log
            sData = String.format("%7d:%7d:%7d:%7d", newFrontLeftTarget, newFrontRightTarget, newBackLeftTarget, newBackRightTarget);
            telemetryAddData("Target", sData);
            sData = String.format("%7d:%7d:%7d:%7d", frontLeft.getCurrentPosition(), frontRight.getCurrentPosition(),
                    backLeft.getCurrentPosition(), backRight.getCurrentPosition());

            telemetryAddData("Actual",sData);
            telemetryUpdate();

            // Turn off RUN_TO_POSITION
            frontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            frontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            backLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            backRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            opMode.sleep(100);
        }

        /*double angleOffSet = Math.abs(initAngle - getAngle());
        if (angleOffSet > 2.0) {
               rotate((int)angleOffSet, 0.6);
        }*/
    }

    private double getMoveCount(double distance, Direction direction){
        double scaleFactor = 1;
        if(RUN_WITH_SCALE_FACTOR) {
            scaleFactor = getScaleFactor(distance, direction);
        }
        double countPerInch = (COUNTS_PER_MOTOR_REV * scaleFactor) /
                (WHEEL_DIAMETER_INCHES * 3.1415 * DRIVE_GEAR_RATIO);
        return (int)(distance * countPerInch);
    }

    private double getScaleFactor(double inches, Direction direction){
        double fwScaleFactor = 1;
        double bwScaleFactor = 1;
        double lfScaleFactor = 1;
        double rtScaleFactor = 1;

        double fwScaleFactor6 = 6.0/5.5; // NEED to check
        double bwScaleFactor6 = 6.0/5.5; // NEED to check
        double lfScaleFactor6 = 6.0/3.7; // NEED to check
        double rtScaleFactor6 = 6.0/3.7; // NEED to check

        double fwScaleFactor12 = 12.0/13.0; // NEED to check
        double bwScaleFactor12 = 12.0/13.0; // NEED to check
        double lfScaleFactor12 = 12.0/8.0; // NEED to check
        double rtScaleFactor12 = 12.0/8.0; // NEED to check

        double fwScaleFactor24 = 24.0/29.5; // NEED to check
        double bwScaleFactor24 = 24.0/28.5; // NEED to check
        double lfScaleFactor24 = 24.0/20; // NEED to check
        double rtScaleFactor24 = 24.0/20; // NEED to check

        double fwScaleFactor36 = 36.0/39.5; // NEED to check
        double bwScaleFactor36 = 36.0/39.0; // NEED to check
        double lfScaleFactor36 = 36.0/30; // NEED more test
        double rtScaleFactor36 = 36.0/30; // NEED more test

        double fwScaleFactor48 = 48.0/51.5; // NEED to check
        double bwScaleFactor48 = 48.0/50.0; // NEED to check
        double lfScaleFactor48 = 48.0/45; // NEED more test
        double rtScaleFactor48 = 48.0/45; // NEED more test

        double fwScaleFactor60 = 60.0/59.0; // NEED to check
        double bwScaleFactor60 = 60.0/59.0; // NEED to check
        double lfScaleFactor60 = 60.0/60; // NEED to check
        double rtScaleFactor60 = 60.0/60; // NEED to check

        double fwScaleFactor72 = 72.0/72.0; // NEED to check
        double bwScaleFactor72 = 72.0/70.0; // NEED to check
        double lfScaleFactor72 = 72.0/72; // NEED to check
        double rtScaleFactor72 = 72.0/72; // NEED to check

        double fwScaleFactor84 = 84.0/80.5; // NEED to check
        double bwScaleFactor84 = 84.0/80.5; // NEED to check
        double lfScaleFactor84 = 84.0/84; // NEED to check
        double rtScaleFactor84 = 84.0/84; // NEED to check

        if (inches < 9) {
            fwScaleFactor = fwScaleFactor6;
            bwScaleFactor = bwScaleFactor6;
            lfScaleFactor = lfScaleFactor6;
            rtScaleFactor = rtScaleFactor6;
        } else if (inches <= 18) {
            fwScaleFactor = fwScaleFactor12;
            bwScaleFactor = bwScaleFactor12;
            lfScaleFactor = lfScaleFactor12;
            rtScaleFactor = rtScaleFactor12;
        } else if (inches <= 30) {
            fwScaleFactor = fwScaleFactor24;
            bwScaleFactor = bwScaleFactor24;
            lfScaleFactor = lfScaleFactor24;
            rtScaleFactor = rtScaleFactor24;
        } else if (inches <= 42) {
            fwScaleFactor = fwScaleFactor36;
            bwScaleFactor = bwScaleFactor36;
            lfScaleFactor = lfScaleFactor36;
            rtScaleFactor = rtScaleFactor36;
        } else if (inches <= 54) {
            fwScaleFactor = fwScaleFactor48;
            bwScaleFactor = bwScaleFactor48;
            lfScaleFactor = lfScaleFactor48;
            rtScaleFactor = rtScaleFactor48;
        } else if (inches <= 66) {
            fwScaleFactor = fwScaleFactor60;
            bwScaleFactor = bwScaleFactor60;
            lfScaleFactor = lfScaleFactor60;
            rtScaleFactor = rtScaleFactor60;
        } else if (inches <= 78) {
            fwScaleFactor = fwScaleFactor72;
            bwScaleFactor = bwScaleFactor72;
            lfScaleFactor = lfScaleFactor72;
            rtScaleFactor = rtScaleFactor72;
        } else {
            fwScaleFactor = fwScaleFactor84;
            bwScaleFactor = bwScaleFactor84;
            lfScaleFactor = lfScaleFactor84;
            rtScaleFactor = rtScaleFactor84;
        }

        if (direction == Direction.FORWARD) {
            return fwScaleFactor;
        } else if (direction == Direction.BACKWARD) {
            return bwScaleFactor;
        } else if (direction == Direction.RIGHT) {
            return  rtScaleFactor;
        } else if (direction == Direction.LEFT) {
            return lfScaleFactor;
        }

        return 1.0;
    }
}
